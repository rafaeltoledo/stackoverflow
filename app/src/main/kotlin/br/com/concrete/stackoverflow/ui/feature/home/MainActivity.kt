package br.com.concrete.stackoverflow.ui.feature.home

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import android.widget.ViewFlipper
import br.com.concrete.stackoverflow.R
import br.com.concrete.stackoverflow.ui.viewmodel.ViewModels

class MainActivity : AppCompatActivity() {

    companion object {
        const val VIEW_LOADING = 0
        const val VIEW_CONTENT = 1
        const val VIEW_ERROR = 2
    }

    lateinit var recyclerView: RecyclerView
    lateinit var error: TextView
    lateinit var viewFlipper: ViewFlipper

    lateinit var viewModel: MainViewModel

    lateinit var adapter: UserAdapter
    lateinit var layoutManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = UserAdapter()
        layoutManager = LinearLayoutManager(this)

        recyclerView = findViewById(R.id.recycler_view)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager

        error = findViewById(R.id.error)
        viewFlipper = findViewById(R.id.view_flipper)

        viewModel = ViewModels.provider.get(this, MainViewModel::class)

        if (savedInstanceState == null) {
            viewModel.loadPage()
        }

        viewModel.error.observe(this, Observer {
            if (it == true) {
                error.text = "Obtivemos um erro. Tente mais tarde."
                viewFlipper.displayedChild = VIEW_ERROR
            }
        })

        viewModel.result.observe(this, Observer {
            adapter.items.addAll(it!!)
            adapter.notifyDataSetChanged()
            viewFlipper.displayedChild = VIEW_CONTENT
        })
    }
}