package br.com.concrete.stackoverflow.ui.feature.home

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import br.com.concrete.stackoverflow.data.api.User

class UserAdapter : RecyclerView.Adapter<UserViewHolder>() {

    val items = mutableListOf<User>()

    override fun onBindViewHolder(holder: UserViewHolder?, position: Int) {
        holder?.bind(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): UserViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        return UserViewHolder(layoutInflater.inflate(
                android.R.layout.simple_list_item_1, parent, false))
    }

    override fun getItemCount() = items.size
}

class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(user: User) {
        val text = itemView.findViewById<TextView>(android.R.id.text1)
        text.text = user.displayName
    }
}
