package br.com.concrete.stackoverflow.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.FragmentActivity
import kotlin.reflect.KClass

class RealViewModelProvider : ViewModelProviderFacade {

    override fun <T : ViewModel> get(host: FragmentActivity, clazz: KClass<T>) =
            ViewModelProviders.of(host).get(clazz.java)
}