package br.com.concrete.stackoverflow.ui.feature.home

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import br.com.concrete.stackoverflow.data.api.ApiCaller
import br.com.concrete.stackoverflow.data.api.ApiResponse
import br.com.concrete.stackoverflow.data.api.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class MainViewModel : ViewModel() {

    open val result = MutableLiveData<List<User>>()
    open val error = MutableLiveData<Boolean>()

    open fun loadPage() {
        ApiCaller.api.users().enqueue(object: Callback<ApiResponse<User>> {
            override fun onResponse(call: Call<ApiResponse<User>>?,
                                    response: Response<ApiResponse<User>>?) {
                if (response?.isSuccessful == true) {
                    result.postValue(response.body()?.items)
                } else {
                    error.postValue(true)
                }
            }

            override fun onFailure(call: Call<ApiResponse<User>>?, t: Throwable?) {
                error.postValue(true)
            }
        })
    }
}