package br.com.concrete.stackoverflow.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.support.v4.app.FragmentActivity
import kotlin.reflect.KClass

interface ViewModelProviderFacade {

    fun <T: ViewModel> get(host: FragmentActivity, clazz: KClass<T>): T
}