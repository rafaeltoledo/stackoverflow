package br.com.concrete.stackoverflow.ui.viewmodel

import android.support.annotation.VisibleForTesting

object ViewModels {

    var provider: ViewModelProviderFacade = RealViewModelProvider()
        private set(value) {
            field = value
        }

    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    fun replaceProvider(fakeProvider: ViewModelProviderFacade) {
        this.provider = fakeProvider
    }
}