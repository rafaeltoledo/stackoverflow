package br.com.concrete.stackoverflow.data.api

import com.squareup.moshi.Json

data class ApiResponse<T>(
        val items: List<T>,
        @field:Json(name = "has_more")
        val hasMore: Boolean
)

data class User(
        @field:Json(name = "display_name")
        val displayName: String
)
