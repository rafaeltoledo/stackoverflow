package br.com.concrete.stackoverflow.data.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface StackExchangeApi {

    @GET("users?order=desc&sort=reputation&site=stackoverflow")
    fun users(@Query("page") page: Int = 1): Call<ApiResponse<User>>
}
