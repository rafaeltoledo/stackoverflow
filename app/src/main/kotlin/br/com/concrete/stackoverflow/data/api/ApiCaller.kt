package br.com.concrete.stackoverflow.data.api

import android.support.annotation.VisibleForTesting
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object ApiCaller {

    var api: StackExchangeApi

    init {
        val client = OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()

        val retrofit = Retrofit.Builder()
                .client(client)
                .baseUrl("https://api.stackexchange.com/2.2/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

        api = retrofit.create(StackExchangeApi::class.java)
    }

    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    fun addTestInterceptor(serverUrl: HttpUrl) {
        val client = OkHttpClient.Builder()
                .addInterceptor {
                    val request = it.request().newBuilder()
                            .url(serverUrl)
                            .build()
                    it.proceed(request)
                }
                .addInterceptor(HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()

        val retrofit = Retrofit.Builder()
                .client(client)
                .baseUrl("https://api.stackexchange.com/2.2/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

        api = retrofit.create(StackExchangeApi::class.java)
    }
}
