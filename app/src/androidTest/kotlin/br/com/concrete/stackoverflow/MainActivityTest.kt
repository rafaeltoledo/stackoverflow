package br.com.concrete.stackoverflow

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import br.com.concrete.stackoverflow.data.api.ApiCaller
import br.com.concrete.stackoverflow.data.api.User
import br.com.concrete.stackoverflow.ui.feature.home.MainActivity
import br.com.concrete.stackoverflow.ui.feature.home.MainViewModel
import br.com.concrete.stackoverflow.ui.viewmodel.ViewModelProviderFacade
import br.com.concrete.stackoverflow.ui.viewmodel.ViewModels
import com.google.common.io.CharStreams
import com.nhaarman.mockito_kotlin.*
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import java.io.InputStreamReader

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @JvmField @Rule
    val activityRule = ActivityTestRule<MainActivity>(MainActivity::class.java,
            true, false)

    @JvmField @Rule
    val server = MockWebServer()

    @Test
    fun shouldDisplayUserListFromLocalServer() {
        val assets = InstrumentationRegistry.getContext().assets
        val body = CharStreams.toString(InputStreamReader(assets.open("json/users.json")))
        server.enqueue(MockResponse().setBody(body))

        ApiCaller.addTestInterceptor(server.url("/"))

        activityRule.launchActivity(Intent())

        onView(withText("Rafael")).check(matches(isDisplayed()))
        onView(withText("Toledo")).check(matches(isDisplayed()))
    }

    @Test
    fun shouldDisplayUserListFromMock() {
        // Mocks ViewModel
        val viewModel = mock(MainViewModel::class.java)
        val error = MutableLiveData<Boolean>()
        error.postValue(false)
        whenever(viewModel.error).thenReturn(error)
        val users = MutableLiveData<List<User>>()
        users.postValue(listOf(User("Rafael"), User("Toledo")))
        whenever(viewModel.result).thenReturn(users)
        doNothing().whenever(viewModel).loadPage()

        val provider = mock(ViewModelProviderFacade::class.java)
        whenever(provider.get<ViewModel>(any(), any())).thenReturn(viewModel)

        ViewModels.replaceProvider(provider)

        activityRule.launchActivity(Intent())

        onView(withText("Rafael")).check(matches(isDisplayed()))
        onView(withText("Toledo")).check(matches(isDisplayed()))
    }
}
